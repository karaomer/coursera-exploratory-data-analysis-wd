# Exploratory Data Analysis
# Assignment 2 / Plot 3

# The reproducible way to download/unzip the data file.
# If the data files are already in the current WD, you can skip this step.
fileName1 <- "Source_Classification_Code.rds"
fileName2 <- "summarySCC_PM25.rds"
fileNameZip <- "exdata-data-NEI_data.zip"
fileDestZip <- paste("./", fileNameZip, sep="")
fileURL <- "https://d396qusza40orc.cloudfront.net/exdata%2Fdata%2FNEI_data.zip"


if (file.exists(fileName1) & file.exists(fileName2)) { 
    message("Both data files already exists in the current WD")
} else {
    if (file.exists(fileNameZip)) {
        message("Unzipping the data folder in the current WD")
        unzip(fileNameZip)
    } else {
        message("Downloading and unzipping the data folder into in the
                        created './data' directory")
        if (!file.exists("./data")) {
            dir.create("./data")
        }
        setwd("./data")
        download.file(fileURL, fileDestZip, method= "auto")
        unzip(fileNameZip)
    }
}

# Read the data
NEI <- readRDS("./summarySCC_PM25.rds") 

# Subset data for Baltimore City.
baltimore <- NEI[NEI$fips == "24510", ] 

# Total emissions by type across years in Baltimore City.
baltimore.type <- aggregate(Emissions ~ type + year, baltimore, FUN = sum)


# Checking the required package: ggplot2
is.installed <- function(mypkg) {
    is.element(mypkg, installed.packages()[,1])
} 
if (!is.installed("ggplot2")) {
    install.packages("ggplot2")
}
library(ggplot2)


# Plot 3
png(file = "plot3.png", bg = "white", width = 1280, height = 800, units = "px")
gplot <- ggplot(baltimore.type, aes(year, Emissions))
gplot + geom_point(aes(color = type), size = 4, alpha = 1) + geom_line(aes(colour = type)) + labs(title = expression('Total PM'[2.5]*' Emissions by Source in Baltimore City from 1999 to 2008'), y = expression('Total PM'[2.5]*' Emission in Tons'), x = "Year") + xlim(1999, 2008)
dev.off()